## How to setup the service-dashboard

You can use Visual Studio 2017 or later to load the solution. There are a couple of setup steps that you will need to complete before being able to run the service dashboard:

**Setup environment variables.**
There is a setup solution folder that contains a setup.bat file. Edit and run this file and adjust the path of the following two environment variables:
- REPO_ROOT_PATH
- VS_MSBUILD_PATH

**Restart Visual Studio After running setup.bat**

This way, the bat files in the scripts folder know where to find the build command, and where to find the repos.

**The app-settings.json file**
This file has contains the metadata for each service. Go through the list and adjust the folder and repo names local to your setup. The branch you will typically leave on develop.

**The App.config file**
Here you set the interval by which service-dashboard will refresh the status of the services. It will look to see if the services are running, and what git branch they are on. As you can see, the appsettings reference a secret file that you need to create alongside the App.config file. Call it AppSecrets.config and have it look similar to this, replacing the values with your credentials:

```xml
   <appSettings>
     <add key="GitUsername" value="you@reliaslearning.com"/>
     <add key="GitPassword" value="password"/>
	 <add key="JiraUserName" value="you@reliaslearning.com" />
	 <add key="JiraPassword" value="password" />
   </appSettings>
```
**The scripts solution folder**
This folder contains all the scripts to launch the services, as defined in the app-settings.json file. If you are having issues launching any of your services, this will be a good resource for troubleshooting.

**Using the service-dashboard**
If everything is setup correctly, you will see a list of services as you defined them in the app-settings.json file. Each service listed will have a command file that is responsible for starting the service. Those bat files are located in the scripts folder, and you point to them from the app-settings.json file with the LaunchCommand property.

Double-Click a service to start it.
Single-Click a service to bring it to the front if it has started.
Right-Click for contextual options to either the selected service, or all services depending on the menu you select.