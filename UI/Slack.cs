﻿using Slack.Webhooks;

namespace Relias.Services.Dashboard
{
    public static class Slack
    {
        public static void PostMessage(string webHookUrl, SlackMessage slackMessage)
        {
            var slackClient = new SlackClient(webHookUrl);
            slackClient.Post(slackMessage);
        }
   }
}