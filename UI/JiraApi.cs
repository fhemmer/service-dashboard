﻿namespace UI
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.IO;
    using System.Net;
    using System.Text;
    using Relias.Services.Dashboard;

    public class JiraApi
    {
        private static string UserName = ConfigurationManager.AppSettings["JiraUserName"];
        private static string Password = ConfigurationManager.AppSettings["JiraPassword"];
        private const string JiraIssueApiUrl = "https://relias.atlassian.net/rest/api/latest/issue/{0}";

        public static void GetFixVersion(Release.Ticket ticket)
        {
            var request = WebRequest.Create(string.Format(JiraIssueApiUrl, ticket.TicketNumber + "?fields=fixVersions,customfield_15201")) as HttpWebRequest;
            if (request == null)
                return;

            request.ContentType = "application/json";
            request.Method = "GET";

            var base64Credentials = EncodeCredentials();
            request.Headers.Add("Authorization", "Basic " + base64Credentials);

            try
            {
                var response = request.GetResponse() as HttpWebResponse;
                var readstring = "";

                if (response == null)
                    return;

                // ReSharper disable once AssignNullToNotNullAttribute
                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    readstring = reader.ReadToEnd();
                }
                var issue = JsonConvert.DeserializeObject<JiraIssue>(readstring);
                StringBuilder fixVersions = new StringBuilder();
                foreach (var version in issue.fields.fixVersions)
                {
                    fixVersions.Append(version.name + ", ");
                }

                ticket.Squad = issue.fields.customField_15201?.value ?? string.Empty;
                ticket.FixVersion = string.IsNullOrEmpty(fixVersions.ToString())
                    ? "**************No Fix Version**************"
                    : fixVersions.ToString().Substring(0, fixVersions.ToString().Length - 2);
            }
            catch (WebException e)
            {
                using (var resp = e.Response)
                {
                    var httpResponse = (HttpWebResponse)resp;
                    var readString = $"Error Code: {httpResponse.StatusCode}";
                    return;
                }
            }
        }

        private static string EncodeCredentials()
        {
            string merge = UserName + ":" + Password;
            var byteCredentials = Encoding.UTF8.GetBytes(merge);
            return Convert.ToBase64String(byteCredentials);
        }

        private class JiraFixVersion
        {
            public string self;
            public string id;
            public string description;
            public string name;
            public string archived;
            public string released;
            public string releaseDate;

            public JiraFixVersion()
            {

            }
        }

        private class JiraSquad
        {
            public string self;
            public string value;
            public string id;

            public JiraSquad()
            {

            }
        }

        private class JiraFields
        {
            public List<JiraFixVersion> fixVersions;
            public JiraSquad customField_15201;

            public JiraFields()
            {
                fixVersions = new List<JiraFixVersion>();
                customField_15201 = new JiraSquad();
            }
        }

        private class JiraIssue
        {
            public string expand;
            public string id;
            public string self;
            public string key;
            public JiraFields fields;

            public JiraIssue()
            {
                fields = new JiraFields();
            }
        }
    }
}
