﻿namespace Relias.Services.Dashboard
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text.RegularExpressions;
    using LibGit2Sharp;
    using UI;

    public static class Release
    {
        public static bool CompareTwoBranches(Service service, string branchName1, string branchName2)
        {
            List<Ticket> tickets = new List<Ticket>();
            var config = new Config();
            var returnValue = false;

            using (var repo = new Repository(service.Folder))
            {
                Git.CheckoutBranch(service, branchName1);
                Git.PullBranch(service, config, branchName1);

                Git.CheckoutBranch(service, branchName2);
                Git.PullBranch(service, config, branchName2);

                var filter = new CommitFilter
                {
                    ExcludeReachableFrom = repo.Branches[branchName1],   // formerly "Since"
                    IncludeReachableFrom = repo.Branches[branchName2],  // formerly "Until"
                };

                const string re1 = "(\\s*)"; // White Space 1
                const string re2 = "(-)"; // Any Single Character 1
                const string re3 = "(\\s*)"; // White Space 2
                const string re4 = "(\\d+)"; // Integer Number 1

                var rlpd = new Regex("(?:RLPD|RAS|PLAT|BC|FC|CHM|QA|MOB)" + re1 + re2 + re3 + re4, RegexOptions.IgnoreCase);
                var commits = repo.Commits.QueryBy(filter);

                foreach (var commit in commits)
                {
                    var rlpdMatch = rlpd.Match(commit.Message);
                    if (!string.IsNullOrEmpty(rlpdMatch.Value) && tickets.All(x => x.TicketNumber != rlpdMatch.Value))
                    {
                        tickets.Add(new Ticket() { TicketNumber = rlpdMatch.Value });
                    }
                    else
                    {
                        // TODO: This should probably go into a list, since it happens that devs forget to put in ticket numbers in commits.
                    }
                }

                // Check JIRA for fix version of each ticket
                foreach (var t in tickets)
                {
                    JiraApi.GetFixVersion(t);
                }

                // TODO: Squad and FixVersion seem to always be empty.
                var csv = string.Join(Environment.NewLine, tickets.Select(d => d.TicketNumber + "|" + d.Squad + "|" + d.FixVersion+ "|"));

                if (tickets.Count > 0 || commits.Any())
                {
                    File.WriteAllText($"Diff-{service.Name}-Branch-{branchName1}-{branchName2}.csv", csv);
                    returnValue = true;
                }

                return returnValue;
            }
        }

        public class Ticket
        {
            public string TicketNumber { get; set; }
            public string FixVersion { get; set; }
            public string Squad { get; set; }
        }
    }
}