﻿namespace Relias.Services.Dashboard
{
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;
    using LibGit2Sharp;
    using LibGit2Sharp.Handlers;

    public static class Git
    {
        public static string CheckoutBranch(Service service, string branchName = "")
        {
            if (string.IsNullOrEmpty(branchName))
            {
                branchName = service.Branch;
            }

            var currentBranch = GetCurrentBranch(service);
            if (currentBranch == branchName)
            {
                return branchName;
            }

            using (var repo = new Repository(service.Folder))
            {
                var trackingBranch = repo.Branches[branchName];
                if (trackingBranch == null)
                {
                    trackingBranch = repo.Branches["origin/" + branchName];
                    if (trackingBranch == null)
                    {
                        return null;
                    }
                }

                if (trackingBranch.IsRemote)
                {
                    branchName = branchName.Replace("origin/", string.Empty);

                    var branch = repo.CreateBranch(branchName, trackingBranch.Tip);
                    repo.Branches.Update(branch, b => b.TrackedBranch = trackingBranch.CanonicalName);
                    Commands.Checkout(repo, branch, new CheckoutOptions { CheckoutModifiers = CheckoutModifiers.Force });
                }
                else
                {
                    Commands.Checkout(repo, trackingBranch, new CheckoutOptions { CheckoutModifiers = CheckoutModifiers.Force });
                }

                return branchName;
            }
        }

        public static Branch CreateAndPushBranch(Config config, Service service, string branchName)
        {
            using (var repo = new Repository(service.Folder))
            {
                // Make sure we don't have the branch already:
                var branch = GetBranch(service, branchName);
                if (branch == null)
                {
                    var createdBranch = repo.CreateBranch(branchName);
                    PushBranch(config, service, createdBranch);
                    return createdBranch;
                }
            }

            return null;
        }

        public static Branch GetBranch(Service service, string branchName, bool local = true)
        {
            using (var repo = new Repository(service.Folder))
            {
                foreach (var branch in repo.Branches)
                {
                    if (string.Compare(branch.FriendlyName, branchName, StringComparison.InvariantCultureIgnoreCase) == 0)
                    {
                        if (local)
                        {
                            if (!branch.IsRemote)
                            {
                                return branch;
                            }
                        }
                        else
                        {
                            if (branch.IsRemote)
                            {
                                return branch;
                            }
                        }
                    }
                }
            }

            return null;
        }

        public static List<string> GetBranches(Service service, bool remoteOnly = false)
        {
            var branches = new List<string>();

            using (var repo = new Repository(service.Folder))
            {
                foreach (var branch in repo.Branches)
                {
                    if (remoteOnly && branch.IsRemote)
                    {
                        branches.Add(branch.FriendlyName);
                    }
                    else
                    {
                        branches.Add(branch.FriendlyName);
                    }
                }
            }

            return branches;
        }

        public static Tuple<int, int> GetCommitsAheadBehind(Service service)
        {
            using (var repo = new Repository(service.Folder))
            {
                foreach (var branch in repo.Branches)
                {
                    if (branch.IsCurrentRepositoryHead)
                    {
                        // If branch does not have a remote b.TrackingDetails.AheadBy and b.TrackingDetails.BehindBy will be both null
                        var commitsAhead = branch.TrackingDetails.AheadBy ?? 0;
                        var commitsBehind = branch.TrackingDetails.BehindBy ?? 0;

                        var commitsAheadBehind = new Tuple<int, int>(commitsAhead, commitsBehind);
                        return commitsAheadBehind;
                    }
                }
            }

            return new Tuple<int, int>(0, 0);
        }

        public static string GetCommitsAheadBehindText(Service service)
        {
            var commitsAheadBehind = GetCommitsAheadBehind(service);
            var returnText = string.Empty;

            if (commitsAheadBehind.Item1 == 0 && commitsAheadBehind.Item2 == 0)
            {
                return returnText;
            }


            if (commitsAheadBehind.Item1 > 0 && commitsAheadBehind.Item2 > 0)
            {
                returnText = $"({commitsAheadBehind.Item1} ahead, {commitsAheadBehind.Item2} behind)";
            }
            else
            {
                returnText = commitsAheadBehind.Item1 > 0 ? $"({commitsAheadBehind.Item1} ahead)" : $"({commitsAheadBehind.Item2} behind)";
            }

            return returnText;
        }

        public static string GetCurrentBranch(Service service)
        {
            using (var repo = new Repository(service.Folder))
            {
                foreach (var branch in repo.Branches)
                {
                    if (branch.IsCurrentRepositoryHead)
                    {
                        return branch.FriendlyName;
                    }
                }
            }

            return null;
        }

        public static void PullBranch(Service service, Config config, string branchName = "")
        {
            if (string.IsNullOrEmpty(branchName))
            {
                branchName = service.Branch;
            }

            CheckoutBranch(service, branchName);

            using (var repo = new Repository(service.Folder))
            {
                var options = new PullOptions
                {
                    MergeOptions = new MergeOptions { FailOnConflict = true },
                    FetchOptions = new FetchOptions
                    {
                        CredentialsProvider = new CredentialsHandler(( url, usernameFromUrl, types) =>
                            CreateUsernamePasswordCredentials(config.GitUserName, config.GitPassword))
                    }
                };

                try
                {
                    Commands.Pull(repo, new Signature(config.GitUserName, config.GitPassword, new DateTimeOffset(DateTime.Now)), options);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Service: " + service.Name + ", Error pulling " + branchName + " -- " + ex.Message);
                    return;
                }

            }
        }

        public static void PushBranch(Config config, Service service, Branch branch)
        {
            using (var repo = new Repository(service.Folder))
            {
                var remote = repo.Network.Remotes["origin"];
                repo.Branches.Update(branch,
                    b => b.Remote = remote.Name,
                    b => b.UpstreamBranch = branch.CanonicalName);


                var options = new PushOptions
                {
                    CredentialsProvider = (url, usernameFromUrl, types) =>
                        CreateUsernamePasswordCredentials(config.GitUserName, config.GitPassword)
                };
                repo.Network.Push(branch, options);
            }
        }

        private static Credentials CreateUsernamePasswordCredentials(string user, string pass)
        {
            return new UsernamePasswordCredentials
            {
                Username = user,
                Password = pass,
            };
        }
    }
}