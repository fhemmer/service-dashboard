﻿namespace UI
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.lvServices = new System.Windows.Forms.ListView();
            this.cmMain = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.gitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cmGitCheckoutBranch = new System.Windows.Forms.ToolStripMenuItem();
            this.cmGitCreateBranch = new System.Windows.Forms.ToolStripMenuItem();
            this.cmGitPull = new System.Windows.Forms.ToolStripMenuItem();
            this.cmGitPullAll = new System.Windows.Forms.ToolStripMenuItem();
            this.cmGitSetAllReposToDevelopAndPull = new System.Windows.Forms.ToolStripMenuItem();
            this.cmGitSetAllReposToMasterAndPull = new System.Windows.Forms.ToolStripMenuItem();
            this.cmRelease = new System.Windows.Forms.ToolStripMenuItem();
            this.cmReleaseCutReleaseBranches = new System.Windows.Forms.ToolStripMenuItem();
            this.cmReleaseCompareTwoBranches = new System.Windows.Forms.ToolStripMenuItem();
            this.cmReleaseTestSlackMessage = new System.Windows.Forms.ToolStripMenuItem();
            this.cmService = new System.Windows.Forms.ToolStripMenuItem();
            this.cmServiceEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.cmServiceStartAll = new System.Windows.Forms.ToolStripMenuItem();
            this.cmServiceStartAllButSelected = new System.Windows.Forms.ToolStripMenuItem();
            this.cmRefresh = new System.Windows.Forms.ToolStripMenuItem();
            this.cmMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // lvServices
            // 
            this.lvServices.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvServices.ContextMenuStrip = this.cmMain;
            this.lvServices.FullRowSelect = true;
            this.lvServices.Location = new System.Drawing.Point(14, 12);
            this.lvServices.Name = "lvServices";
            this.lvServices.Size = new System.Drawing.Size(1446, 492);
            this.lvServices.TabIndex = 0;
            this.lvServices.UseCompatibleStateImageBehavior = false;
            this.lvServices.View = System.Windows.Forms.View.Details;
            this.lvServices.DoubleClick += new System.EventHandler(this.lvServices_DoubleClick);
            this.lvServices.MouseClick += new System.Windows.Forms.MouseEventHandler(this.lvServices_MouseClick);
            // 
            // cmMain
            // 
            this.cmMain.ImageScalingSize = new System.Drawing.Size(28, 28);
            this.cmMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gitToolStripMenuItem,
            this.cmRelease,
            this.cmService,
            this.cmRefresh});
            this.cmMain.Name = "cmMain";
            this.cmMain.Size = new System.Drawing.Size(222, 178);
            // 
            // gitToolStripMenuItem
            // 
            this.gitToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cmGitCheckoutBranch,
            this.cmGitCreateBranch,
            this.cmGitPull,
            this.cmGitPullAll,
            this.cmGitSetAllReposToDevelopAndPull,
            this.cmGitSetAllReposToMasterAndPull});
            this.gitToolStripMenuItem.Name = "gitToolStripMenuItem";
            this.gitToolStripMenuItem.Size = new System.Drawing.Size(221, 34);
            this.gitToolStripMenuItem.Text = "Git";
            // 
            // cmGitCheckoutBranch
            // 
            this.cmGitCheckoutBranch.Name = "cmGitCheckoutBranch";
            this.cmGitCheckoutBranch.Size = new System.Drawing.Size(402, 34);
            this.cmGitCheckoutBranch.Text = "Checkout branch...";
            this.cmGitCheckoutBranch.Click += new System.EventHandler(this.cmGitCheckoutBranch_Click);
            // 
            // cmGitCreateBranch
            // 
            this.cmGitCreateBranch.Name = "cmGitCreateBranch";
            this.cmGitCreateBranch.Size = new System.Drawing.Size(402, 34);
            this.cmGitCreateBranch.Text = "Create branch...";
            this.cmGitCreateBranch.Click += new System.EventHandler(this.cmGitCreateBranch_Click);
            // 
            // cmGitPull
            // 
            this.cmGitPull.Name = "cmGitPull";
            this.cmGitPull.Size = new System.Drawing.Size(402, 34);
            this.cmGitPull.Text = "Pull";
            this.cmGitPull.Click += new System.EventHandler(this.cmGitPull_Click);
            // 
            // cmGitPullAll
            // 
            this.cmGitPullAll.Name = "cmGitPullAll";
            this.cmGitPullAll.Size = new System.Drawing.Size(402, 34);
            this.cmGitPullAll.Text = "Pull All";
            this.cmGitPullAll.Click += new System.EventHandler(this.cmGitPullAll_Click);
            // 
            // cmGitSetAllReposToDevelopAndPull
            // 
            this.cmGitSetAllReposToDevelopAndPull.Name = "cmGitSetAllReposToDevelopAndPull";
            this.cmGitSetAllReposToDevelopAndPull.Size = new System.Drawing.Size(402, 34);
            this.cmGitSetAllReposToDevelopAndPull.Text = "Set all repos to develop and pull";
            this.cmGitSetAllReposToDevelopAndPull.Click += new System.EventHandler(this.cmGitSetAllReposToDevelopAndPull_Click_1);
            // 
            // cmGitSetAllReposToMasterAndPull
            // 
            this.cmGitSetAllReposToMasterAndPull.Name = "cmGitSetAllReposToMasterAndPull";
            this.cmGitSetAllReposToMasterAndPull.Size = new System.Drawing.Size(402, 34);
            this.cmGitSetAllReposToMasterAndPull.Text = "Set all repos to master and pull";
            this.cmGitSetAllReposToMasterAndPull.Click += new System.EventHandler(this.cmGitSetAllReposToMasterAndPull_Click);
            // 
            // cmRelease
            // 
            this.cmRelease.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cmReleaseCutReleaseBranches,
            this.cmReleaseCompareTwoBranches,
            this.cmReleaseTestSlackMessage});
            this.cmRelease.Name = "cmRelease";
            this.cmRelease.Size = new System.Drawing.Size(221, 34);
            this.cmRelease.Text = "Release";
            // 
            // cmReleaseCutReleaseBranches
            // 
            this.cmReleaseCutReleaseBranches.Name = "cmReleaseCutReleaseBranches";
            this.cmReleaseCutReleaseBranches.Size = new System.Drawing.Size(494, 34);
            this.cmReleaseCutReleaseBranches.Text = "Cut release branches for selected services.";
            this.cmReleaseCutReleaseBranches.Click += new System.EventHandler(this.cmReleaseCutReleaseBranches_Click);
            // 
            // cmReleaseCompareTwoBranches
            // 
            this.cmReleaseCompareTwoBranches.Name = "cmReleaseCompareTwoBranches";
            this.cmReleaseCompareTwoBranches.Size = new System.Drawing.Size(494, 34);
            this.cmReleaseCompareTwoBranches.Text = "Compare two branches";
            this.cmReleaseCompareTwoBranches.Click += new System.EventHandler(this.cmReleaseCompareTwoBranches_Click);
            // 
            // cmReleaseTestSlackMessage
            // 
            this.cmReleaseTestSlackMessage.Name = "cmReleaseTestSlackMessage";
            this.cmReleaseTestSlackMessage.Size = new System.Drawing.Size(494, 34);
            this.cmReleaseTestSlackMessage.Text = "Test Slack Message";
            this.cmReleaseTestSlackMessage.Click += new System.EventHandler(this.cmReleaseTestSlackMessage_Click);
            // 
            // cmService
            // 
            this.cmService.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cmServiceEdit,
            this.cmServiceStartAll,
            this.cmServiceStartAllButSelected});
            this.cmService.Name = "cmService";
            this.cmService.Size = new System.Drawing.Size(221, 34);
            this.cmService.Text = "Service";
            // 
            // cmServiceEdit
            // 
            this.cmServiceEdit.Name = "cmServiceEdit";
            this.cmServiceEdit.Size = new System.Drawing.Size(375, 34);
            this.cmServiceEdit.Text = "Edit";
            this.cmServiceEdit.Click += new System.EventHandler(this.cmServiceEdit_Click);
            // 
            // cmServiceStartAll
            // 
            this.cmServiceStartAll.Name = "cmServiceStartAll";
            this.cmServiceStartAll.Size = new System.Drawing.Size(375, 34);
            this.cmServiceStartAll.Text = "Start All";
            this.cmServiceStartAll.Click += new System.EventHandler(this.cmServiceStartAll_Click);
            // 
            // cmServiceStartAllButSelected
            // 
            this.cmServiceStartAllButSelected.Name = "cmServiceStartAllButSelected";
            this.cmServiceStartAllButSelected.Size = new System.Drawing.Size(375, 34);
            this.cmServiceStartAllButSelected.Text = "Start All but selected Services";
            this.cmServiceStartAllButSelected.Click += new System.EventHandler(this.cmServiceStartAllButSelected_Click);
            // 
            // cmRefresh
            // 
            this.cmRefresh.Name = "cmRefresh";
            this.cmRefresh.Size = new System.Drawing.Size(221, 34);
            this.cmRefresh.Text = "Refresh";
            this.cmRefresh.Click += new System.EventHandler(this.cmRefresh_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1472, 516);
            this.Controls.Add(this.lvServices);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.cmMain.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView lvServices;
        private System.Windows.Forms.ContextMenuStrip cmMain;
        private System.Windows.Forms.ToolStripMenuItem cmService;
        private System.Windows.Forms.ToolStripMenuItem cmServiceStartAll;
        private System.Windows.Forms.ToolStripMenuItem cmServiceStartAllButSelected;
        private System.Windows.Forms.ToolStripMenuItem gitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cmGitSetAllReposToDevelopAndPull;
        private System.Windows.Forms.ToolStripMenuItem cmGitCheckoutBranch;
        private System.Windows.Forms.ToolStripMenuItem cmRefresh;
        private System.Windows.Forms.ToolStripMenuItem cmGitSetAllReposToMasterAndPull;
        private System.Windows.Forms.ToolStripMenuItem cmGitPull;
        private System.Windows.Forms.ToolStripMenuItem cmServiceEdit;
        private System.Windows.Forms.ToolStripMenuItem cmGitPullAll;
        private System.Windows.Forms.ToolStripMenuItem cmRelease;
        private System.Windows.Forms.ToolStripMenuItem cmReleaseCompareTwoBranches;
        private System.Windows.Forms.ToolStripMenuItem cmReleaseTestSlackMessage;
        private System.Windows.Forms.ToolStripMenuItem cmGitCreateBranch;
        private System.Windows.Forms.ToolStripMenuItem cmReleaseCutReleaseBranches;
    }
}

