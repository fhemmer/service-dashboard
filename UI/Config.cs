﻿namespace Relias.Services.Dashboard
{
    using System.Collections.Generic;
    using System.Configuration;
    using System.IO;
    using Newtonsoft.Json;

    public class Config
    {
        public string GitUserName { get; set; }
        public string GitPassword { get; set; }
        public int RefreshIntervalInSeconds { get; set; }
        public List<Service> Services { get; set; }
        public string SlackUrl { get; set; }

        public Config()
        {
            var jsonText = File.ReadAllText(@"app-settings.json");
            Services = JsonConvert.DeserializeObject<List<Service>>(jsonText);
            RefreshIntervalInSeconds = int.Parse(ConfigurationManager.AppSettings["RefreshIntervalInSeconds"]);
            GitUserName = ConfigurationManager.AppSettings["GitUserName"];
            GitPassword = ConfigurationManager.AppSettings["GitPassword"];
            SlackUrl = ConfigurationManager.AppSettings["SlackUrl"];
        }

        public void Save()
        {
            using (TextWriter tw = new StreamWriter(@"app-settings.json"))
            {
                var s = new JsonSerializer();
                s.Serialize(tw, Services);
                tw.Close();
            }
        }
    }
}
