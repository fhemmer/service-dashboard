﻿using Slack.Webhooks;

namespace UI
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Windows.Forms;
    using Microsoft.VisualBasic;
    using Relias.Services.Dashboard;
    using Relias.Services.Dashboard.Forms;
    using UI.Properties;

    public partial class MainForm : Form
    {
        private readonly Config _config;

        public MainForm()
        {
            InitializeComponent();
            _config = new Config();

            this.Text = @"Service Dashboard V1.01";
            this.CenterToScreen();

            var mainTimer = new Timer { Interval = _config.RefreshIntervalInSeconds * 1000 };
            mainTimer.Tick += MainTimerOnTick;
            mainTimer.Start();
        }

        private void MainTimerOnTick(object sender, EventArgs eventArgs)
        {
            PopulateServicesListView();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            this.Left = Settings.Default.MainLeft;
            this.Top = Settings.Default.MainTop;
            this.Width = Settings.Default.MainWidth;
            this.Height = Settings.Default.MainHeight;

            var ch = lvServices.Columns.Add("Name");
            ch.Width = Settings.Default.ListViewColumn1Width;
            ch = lvServices.Columns.Add("Folder");
            ch.Width = Settings.Default.ListViewColumn2Width;
            ch = lvServices.Columns.Add("Branch");
            ch.Width = Settings.Default.ListViewColumn3Width;
            ch = lvServices.Columns.Add("Launch Command");
            ch.Width = Settings.Default.ListViewColumn4Width;
            ch = lvServices.Columns.Add("Running");
            ch.Width = Settings.Default.ListViewColumn5Width;

            PopulateServicesListView();
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Settings.Default.MainLeft = this.Left;
            Settings.Default.MainTop = this.Top;
            Settings.Default.MainWidth = this.Width;
            Settings.Default.MainHeight = this.Height;

            Settings.Default.ListViewColumn1Width = lvServices.Columns[0].Width;
            Settings.Default.ListViewColumn2Width = lvServices.Columns[1].Width;
            Settings.Default.ListViewColumn3Width = lvServices.Columns[2].Width;
            Settings.Default.ListViewColumn4Width = lvServices.Columns[3].Width;
            Settings.Default.ListViewColumn5Width = lvServices.Columns[4].Width;

            Settings.Default.Save();
        }

        private void Busy()
        {
            Cursor.Current = Cursors.WaitCursor;
        }

        private void NotBusy()
        {
            Cursor.Current = Cursors.Default;
        }

        private void PopulateServicesListView()
        {
            Busy();

            lvServices.BeginUpdate();
            lvServices.Items.Clear();

            foreach (var service in _config.Services)
            {
                var lvi = new ListViewItem { Text = service.Name };
                lvi.Tag = service;
                lvi.SubItems.Add(service.Folder);

                var currentBranch = Git.GetCurrentBranch(service);
                service.Branch = currentBranch;
                var commitsAheadBehind = Git.GetCommitsAheadBehindText(service);
                if (!string.IsNullOrEmpty(commitsAheadBehind))
                {
                    currentBranch += " " + commitsAheadBehind;
                }

                lvi.SubItems.Add(currentBranch);
                lvi.SubItems.Add(service.LaunchCommand);

                var running = "No";
                if (string.IsNullOrEmpty(service.LaunchCommand))
                {
                    running = "--";
                }
                else
                {
                    if (service.ProcessId > 0)
                    {
                        var process = Process.GetProcesses().FirstOrDefault(x => x.Id == service.ProcessId);
                        if (process != null)
                        {
                            running = "Yes";
                        }
                    }
                }

                lvi.SubItems.Add(running);
                lvServices.Items.Add(lvi);
            }

            lvServices.EndUpdate();
            NotBusy();
        }

        private void cmGitCheckoutBranch_Click(object sender, EventArgs e)
        {
            var selectedBranchName = string.Empty;

            if (lvServices.SelectedItems.Count == 0)
            {
                MessageBox.Show("Please select a service.");
                return;
            }

            // Get the service that was clicked:
            var selectedService = (Service)lvServices.SelectedItems[0].Tag;

            using (var branchPickerForm = new BranchPickerForm(selectedService, true))
            {
                branchPickerForm.ShowDialog();
                selectedBranchName = branchPickerForm.BranchSelected;
                if (string.IsNullOrEmpty(selectedBranchName))
                {
                    return;
                }
            }

            Busy();


            Git.PullBranch(selectedService, _config, selectedBranchName);

            NotBusy();
            PopulateServicesListView();
        }

        private void cmGitCreateBranch_Click(object sender, EventArgs e)
        {
            try
            {
                // Get the service that was selected:
                var service = (Service)lvServices.SelectedItems[0].Tag;
                var branchName = Interaction.InputBox("Enter branch name:", $"Create Branch - { service.Name }");
                var branch = Git.CreateAndPushBranch(_config, service, branchName);
                if (branch != null)
                {
                    Git.PullBranch(service, _config, branchName);
                    MessageBox.Show($"{branch.FriendlyName} has been created and pushed to origin.");
                }
                else
                {
                    MessageBox.Show("Branch already exists.");
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.ToString());
                throw;
            }
        }

        private void cmGitPull_Click(object sender, EventArgs e)
        {
            Busy();

            var services = lvServices.SelectedItems;
            foreach (var service in services)
            {
                var svc = (Service) ((ListViewItem) service).Tag;
                Git.PullBranch(svc, _config);
            }

            NotBusy();
            PopulateServicesListView();
        }

        private void cmGitPullAll_Click(object sender, EventArgs e)
        {
            Busy();

            foreach (var service in _config.Services)
            {
                Git.PullBranch(service, _config);
            }

            NotBusy();
            PopulateServicesListView();
        }

        private void cmGitSetAllReposToDevelopAndPull_Click_1(object sender, EventArgs e)
        {
            Busy();

            foreach (ListViewItem li in lvServices.Items)
            {
                var service = (Service)li.Tag;
                service.Branch = "develop";
                Git.PullBranch(service, _config);
            }

            NotBusy();
            MessageBox.Show("Done pulling branches in all services.");
            PopulateServicesListView();
        }

        private void cmGitSetAllReposToMasterAndPull_Click(object sender, EventArgs e)
        {
            Busy();

            foreach (ListViewItem li in lvServices.Items)
            {
                var service = (Service) li.Tag;
                service.Branch = "master";
                Git.PullBranch(service, _config);
            }

            NotBusy();
            MessageBox.Show("Done pulling branches in all services.");
            PopulateServicesListView();
        }

        private void cmRefresh_Click(object sender, EventArgs e)
        {
            PopulateServicesListView();
        }

        private void cmReleaseCutReleaseBranches_Click(object sender, EventArgs e)
        {
            var services = lvServices.SelectedItems;
            if (services.Count > 0)
            {
                if (MessageBox.Show(
                        $"You are cutting release branches from develop for {services.Count} repositories. Are you sure?",
                        "Cutting release branch(es)", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Exclamation,
                        MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    var reposProcessed = new List<string>();
                    var branchName = Interaction.InputBox
                    (
                        "Enter release branch name:", $"Create release branch(es)", $"release/" +
                        $"{DateTime.Now.Year}.{DateTime.Now.Month:00}.{DateTime.Now.Day:00}"
                    );

                    if (string.IsNullOrEmpty(branchName))
                    {
                        return;
                    }

                    Busy();

                    foreach (var service in services)
                    {
                        var svc = (Service) ((ListViewItem)service).Tag;
                        Git.CreateAndPushBranch(_config, svc, branchName);
                        reposProcessed.Add(svc.Name);
                    }

                    NotBusy();

                    MessageBox.Show($"Release branch(es) cut for {services.Count} repositories.");

                    if (MessageBox.Show(
                            $"You want to notify the #nextrelease channel that release branches have been cut?",
                            "Notify release channel?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Exclamation,
                            MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                    {
                        var slackMessageText = $"Release branch *{branchName}* has been created for the following repos:" + Environment.NewLine;
                        foreach (var repo in reposProcessed)
                        {
                            slackMessageText += repo + Environment.NewLine;
                        }

                        var slackMessage = new SlackMessage
                        {
                            Channel = "#nextrelease",
                            Text = slackMessageText,
                            Username = "Service Dashboard",
                            Markdown = true
                        };

                        Slack.PostMessage(_config.SlackUrl, slackMessage);
                    }

                    PopulateServicesListView();
                    return;
                }
            }

            MessageBox.Show("Please select at least one service.");
            PopulateServicesListView();
        }

        private void cmReleaseCompareTwoBranches_Click(object sender, EventArgs e)
        {
            if (lvServices.SelectedItems.Count == 0)
            {
                MessageBox.Show("Please select a service and try again.");
                return;
            }


            var sourceBranchName = Interaction.InputBox("Enter source branch name:", "Source Branch", "develop");
            var destinationBranchName = Interaction.InputBox("Enter destination branch name:", "Destination Branch", "master");

            Busy();

            var services = lvServices.SelectedItems;
            var reposThatChanged = new List<Service>();
            this.Refresh();
            foreach (var service in services)
            {
                var svc = (Service) ((ListViewItem) service).Tag;
                if (Release.CompareTwoBranches(svc, destinationBranchName, sourceBranchName))
                {
                    reposThatChanged.Add(svc);
                }
            }

            NotBusy();

            if (reposThatChanged.Count == 0)
            {
                MessageBox.Show($"No changes found in { services.Count } repositories between branches {sourceBranchName} and {destinationBranchName}.");
            }
            else
            {
                MessageBox.Show($"Found { reposThatChanged.Count } changes in {services.Count} repositories. Diff files have been produced.between branches {sourceBranchName} and {destinationBranchName}.");
            }

            PopulateServicesListView();
        }

        private void cmReleaseTestSlackMessage_Click(object sender, EventArgs e)
        {
            var message = new SlackMessage
            {
                Username = "Service Dashboard",
                Text = "This is a test message from the Service Dashboard."
            };
            Slack.PostMessage(_config.SlackUrl, message);
        }

        private void cmServiceEdit_Click(object sender, EventArgs e)
        {
            Busy();

            // Get the service that was clicked:
            var service = (Service)lvServices.SelectedItems[0].Tag;
            service.EditService();

            NotBusy();
            PopulateServicesListView();
        }

        private void cmServiceStartAll_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem li in lvServices.Items)
            {
                var service = (Service)li.Tag;
                service.StartService();
            }
            PopulateServicesListView();
        }

        private void cmServiceStartAllButSelected_Click(object sender, EventArgs e)
        {
            var allServiceNames = new List<string>();
            foreach (ListViewItem li in lvServices.Items)
            {
                var service = (Service)li.Tag;
                allServiceNames.Add(service.Name);
            }

            var selectedServiceNames = new List<string>();
            var services = lvServices.SelectedItems;
            foreach (var service in services)
            {
                selectedServiceNames.Add(((Service) ((ListViewItem) service).Tag).Name);
            }

            var listToProcess = allServiceNames.Where(all => selectedServiceNames.All(selected => selected != all)).ToArray();
            this.Refresh();

            foreach (ListViewItem li in lvServices.Items)
            {
                var service = (Service)li.Tag;
                if (listToProcess.Contains(service.Name))
                {
                    service.StartService();
                    this.Refresh();
                }
            }

            PopulateServicesListView();
        }

        private void lvServices_DoubleClick(object sender, EventArgs e)
        {
            // Get the service that was double-clicked:
            if (lvServices.SelectedItems.Count == 0)
            {
                return;
            }

            var service = (Service)lvServices.SelectedItems[0].Tag;
            service.StartService();
            PopulateServicesListView();
        }

        private void lvServices_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                return;
            }

            // Get the service that was clicked:
            var service = (Service)lvServices.SelectedItems[0].Tag;

            if (service.ProcessId > 0)
            {
                var process = Process.GetProcesses().FirstOrDefault(x => x.Id == service.ProcessId);
                if (process != null)
                {
                    WindowHelper.BringProcessToFront(process);
                }

                PopulateServicesListView();
            }
        }
    }
}
