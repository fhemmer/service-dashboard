﻿namespace Relias.Services.Dashboard
{
    using System.Diagnostics;
    using System.Linq;

    using Newtonsoft.Json;

    // ReSharper disable once ClassNeverInstantiated.Global
    public class Service
    {
        public string Name { get; set; }
        public string Folder { get; set; }
        public string Branch { get; set; }
        public string EditCommand { get; set; }
        public string LaunchCommand { get; set; }

        [JsonIgnore]
        public int ProcessId { get; set; }


        public void EditService()
        {
            // It is possible to have a service without a launch command in which case we just exit.
            if (string.IsNullOrEmpty(EditCommand))
            {
                return;
            }

            // Make sure we haven't already launched this service:
            if (ProcessId > 0)
            {
                var process = Process.GetProcesses().FirstOrDefault(x => x.Id == ProcessId);
                if (process != null)
                {
                    return;
                }
            }

            using (var process = new Process())
            {
                process.StartInfo.FileName = EditCommand;
                process.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
                process.StartInfo.UseShellExecute = true;
                process.StartInfo.RedirectStandardInput = false;
                process.StartInfo.RedirectStandardOutput = false;
                process.StartInfo.RedirectStandardError = false;

                process.Start();
                ProcessId = process.Id;
            }
        }

        public void StartService()
        {
            // It is possible to have a service without a launch command in which case we just exit.
            if (string.IsNullOrEmpty(LaunchCommand))
            {
                return;
            }

            // Make sure we haven't already launched this service:
            if (ProcessId > 0)
            {
                var process = Process.GetProcesses().FirstOrDefault(x => x.Id == ProcessId);
                if (process != null)
                {
                    return;
                }
            }

            using (var process = new Process())
            {
                process.StartInfo.FileName = LaunchCommand;
                process.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
                process.StartInfo.UseShellExecute = true;
                process.StartInfo.RedirectStandardInput = false;
                process.StartInfo.RedirectStandardOutput = false;
                process.StartInfo.RedirectStandardError = false;

                process.Start();
                ProcessId = process.Id;
            }
        }
    }
}