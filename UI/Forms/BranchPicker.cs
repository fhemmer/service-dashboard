﻿namespace Relias.Services.Dashboard.Forms
{
    using System;
    using System.Windows.Forms;

    public partial class BranchPickerForm : Form
    {
        public string BranchSelected { get; set; }
        private bool _remoteOnly;
        private Service _service;

        public BranchPickerForm(Service service, bool remoteOnly = false)
        {
            _service = service;
            _remoteOnly = remoteOnly;

            InitializeComponent();
        }

        private void BranchPickerForm_Load(object sender, EventArgs e)
        {
            var branches = Git.GetBranches(_service, _remoteOnly);
            foreach (var branch in branches)
            {
                lbBranches.Items.Add(branch);
            }
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            if (lbBranches.SelectedItems.Count == 0)
            {
                MessageBox.Show("Please select a branch.");
                return;
            }

            BranchSelected = lbBranches.SelectedItem.ToString();
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            BranchSelected = string.Empty;
            this.Close();
        }
    }
}
