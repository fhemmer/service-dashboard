TITLE Identity Server
cd %REPO_ROOT_PATH%\IdentityService\src\Relias.Identity.Server
dotnet restore && dotnet ef database update --context PersistedGrantDbContext
npm install && node_modules\.bin\gulp && dotnet run
PAUSE