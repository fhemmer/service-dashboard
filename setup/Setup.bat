@ECHO OFF

:: Do not append \ to any paths defined here.

:: Repo Root Path. 
SETX REPO_ROOT_PATH "C:\git"

:: Visual Studio MSBuild folder path:
SETX VS_MSBUILD_PATH "C:\Program Files (x86)\Microsoft Visual Studio\2017\Enterprise\MSBuild\15.0\bin"
SETX VS_BIN_PATH "C:\Program Files (x86)\Microsoft Visual Studio\2017\Enterprise\Common7\IDE\devenv.exe"

ECHO REPO_ROOT_PATH set to "C:\git"
ECHO VS_MSBUILD_PATH set to "C:\Program Files (x86)\Microsoft Visual Studio\2017\Enterprise\MSBuild\15.0\bin"
ECHO:
ECHO Edit this bat file if you want to adjust the path.
ECHO:
PAUSE